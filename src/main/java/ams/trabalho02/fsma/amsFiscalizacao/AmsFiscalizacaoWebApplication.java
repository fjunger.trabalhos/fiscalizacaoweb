package ams.trabalho02.fsma.amsFiscalizacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmsFiscalizacaoWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmsFiscalizacaoWebApplication.class, args);
	}

}
