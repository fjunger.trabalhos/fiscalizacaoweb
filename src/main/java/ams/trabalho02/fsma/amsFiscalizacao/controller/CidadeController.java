package ams.trabalho02.fsma.amsFiscalizacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;
import ams.trabalho02.fsma.amsFiscalizacao.service.CidadeService;
import ams.trabalho02.fsma.amsFiscalizacao.service.UfService;

@Controller
@RequestMapping("/cidade")
public class CidadeController {

	@Autowired UfService ufservice;
	@Autowired CidadeService service;
	
	@GetMapping("/listatodos")
	public String listAll(ModelMap model) {
		model.addAttribute("cidades", service.list());
		return "cidade/listar";
	}
	
	@GetMapping("/buscar/nome")
	public String buscaPorNome(@RequestParam("nome") String nome,ModelMap model) {
		model.addAttribute("cidades", service.getByName(removerCharEspec(nome)));
		return "cidade/listar";
	}
	
	@GetMapping("/buscar/uf")
	public String buscaPorUf(@RequestParam("id") String ufId,ModelMap model) {
		model.addAttribute("cidades", service.getByUfId(Long.valueOf(ufId)));
		return "cidade/listar";
	}

	private String removerCharEspec(String nome) {
		return nome
				.trim()
				.toUpperCase()
				.replace("Ç", "C")
				.replace("Ã", "A")
				.replace("Á", "A")
				.replace("Â", "A")
				.replace("Ú", "U")
				.replace("É", "E")
				.replace("Í", "I")
				.replace("Ê", "E")
				.replace(" ", "_");
	}
	
	@ModelAttribute("uflist")
    public List<UnidadeFederativa> listaDepartamentos(){
        return ufservice.list();
    }
	
	
	@GetMapping("/cadastrar")
	public String cadastrarCidade(Cidade cidade,ModelMap model) {
		model.addAttribute("cidade", cidade);
		return "cidade/cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvarCidade(@Valid Cidade cidade, BindingResult result, RedirectAttributes attr,
			ModelMap model) {
		if (result.hasErrors()) {
			return "cidade/cadastro";
		}
		
		if(verificaDuplicidade(cidade,model)) {
			return "cidade/cadastro";
		}

		service.persist(cidade);
		attr.addFlashAttribute("success", "cidade inserida com sucesso");
		return "redirect:/cidade/cadastrar";
	}

	private boolean verificaDuplicidade(@Valid Cidade cidade, ModelMap model) {
		if(service.getByNameAndUf(cidade) != null) {
			model.addAttribute("fail", "Já existe cidade cadastrada com este nome: " + cidade.getNome() + " nesta U.F. " + cidade.getUf().getName()); 
			return true;
		}
		return false;
	}
	
	@PostMapping("/remover")
	private String excluir(@RequestParam("id") String id, ModelMap model) {
			if (service.cidadeTemBairros(Long.valueOf(id))) {
				model.addAttribute("fail", "cidade não removida. Possui bairro(s) vinculado(s).");
			} else {
				service.remove(Long.valueOf(id));
	            model.addAttribute("success", "Cidade excluída com sucesso.");
			}
			return listAll(model);
	}
	
	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Long id, ModelMap model) {
		System.out.println(service.getById(id).getId());
		model.addAttribute("cidade", service.getById(id));
		return "cidade/cadastro";
	}
	
	@PostMapping("/editar")
	public String editar(@Valid Cidade cidade, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "cidade/cadastro";
		}
		service.persist(cidade);
		attr.addFlashAttribute("success", "Cidade editado com sucesso");
		return "redirect:/cidade/listatodos";
	}
	
}
