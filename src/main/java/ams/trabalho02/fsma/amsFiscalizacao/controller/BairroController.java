package ams.trabalho02.fsma.amsFiscalizacao.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ams.trabalho02.fsma.amsFiscalizacao.model.Bairro;
import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.service.BairroService;
import ams.trabalho02.fsma.amsFiscalizacao.service.CidadeService;
import ams.trabalho02.fsma.amsFiscalizacao.service.UfService;

@Controller
@RequestMapping("bairro")
public class BairroController {
	
	@Autowired private BairroService service;
	@Autowired private UfService ufservice;
	@Autowired private CidadeService cidadeService;

	@GetMapping("/listatodos")
	public String listAll(ModelMap model) {
		model.addAttribute("uflist",ufservice.list());
		model.addAttribute("cidades", cidadeService.list());
		model.addAttribute("bairros", service.list());
		return "bairro/listar";
	}
	
	@GetMapping("/buscar/nome")
	public String buscaPorNome(@RequestParam("nome") String nome,ModelMap model) {
		model.addAttribute("bairros", service.getByName(removerCharEspec(nome)));
		model.addAttribute("uflist",ufservice.list());
		model.addAttribute("cidades", cidadeService.list());
		return "bairro/listar";
	}
	
	@GetMapping("/buscar/uf")
	public String buscaPorUf(@RequestParam("id") String uf,ModelMap model) {
		model.addAttribute("bairros", service.getByUf(Long.valueOf(uf)));
		model.addAttribute("uflist",ufservice.list());
		model.addAttribute("cidades", cidadeService.list());
		return "bairro/listar";
	}
	
	@GetMapping("buscar/cidade")
	public String buscaPorCidade(@RequestParam("id") String cidade,ModelMap model) {
		model.addAttribute("bairros", service.getByCidade(Long.valueOf(cidade)));
		model.addAttribute("uflist",ufservice.list());
		model.addAttribute("cidades", cidadeService.listOrdered());
		return "bairro/listar";
	}
	
	@GetMapping("/cadastrar")
	public String cadastrar(Bairro bairro,ModelMap model) {
		model.addAttribute("bairro", bairro);
		model.addAttribute("cidadelist", cidadeService.listOrdered());
		model.addAttribute("uflist", ufservice.listOrdered());
		return "bairro/cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvar(@Valid Bairro bairro, BindingResult result, RedirectAttributes attr,
			ModelMap model) {
		bairro.setNome(removerCharEspec(bairro.getNome()));
		if (result.hasErrors()) {
			model.addAttribute("cidadelist", cidadeService.listOrdered());
			model.addAttribute("uflist", ufservice.listOrdered());
			return "bairro/cadastro";
		}
		
		if(verifyDuplicity(bairro,model)) {
			model.addAttribute("cidadelist", cidadeService.listOrdered());
			model.addAttribute("uflist", ufservice.listOrdered());
			return "bairro/cadastro";
		}

		service.persist(bairro);
		attr.addFlashAttribute("success", "Cidade inserida com sucesso");
		return "redirect:/bairro/cadastrar";
	}

	private boolean verifyDuplicity(Bairro bairro,ModelMap model) {
		if(service.exist(bairro)){
			model.addAttribute("fail", "Já existe Bairro cadastrado com o  nome " + bairro.getNome() + " nesta Cidade " + bairro.getCidade().getNome());
			return true;
		}
		return false;
	}

	@PostMapping("/remover")
	public String remover(@RequestParam("id") String id, ModelMap model) {
		if (service.BairroTemEmpresa(Long.valueOf(id))) {
			model.addAttribute("fail", "Bairro não removido. Possui empresa(s) vinculada(s).");
		} else {
			service.remove(Long.valueOf(id));
            model.addAttribute("success", "Bairro excluído com sucesso.");
		}
		return listAll(model);
	}
	
	private String removerCharEspec(String nome) {
		return nome
				.trim()
				.toUpperCase()
				.replace("Ç", "C")
				.replace("Ã", "A")
				.replace("Á", "A")
				.replace("Â", "A")
				.replace("Ú", "U")
				.replace("É", "E")
				.replace("Í", "I")
				.replace("Ê", "E")
				.replace(" ", "_");
	}
	
}
