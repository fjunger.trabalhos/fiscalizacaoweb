package ams.trabalho02.fsma.amsFiscalizacao.controller;

import java.time.LocalDate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ams.trabalho02.fsma.amsFiscalizacao.model.Bairro;
import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.model.Empresa;
import ams.trabalho02.fsma.amsFiscalizacao.model.Fiscalizacao;
import ams.trabalho02.fsma.amsFiscalizacao.service.BairroService;
import ams.trabalho02.fsma.amsFiscalizacao.service.CidadeService;
import ams.trabalho02.fsma.amsFiscalizacao.service.EmpresaService;
import ams.trabalho02.fsma.amsFiscalizacao.service.FiscalizacaoService;
import ams.trabalho02.fsma.amsFiscalizacao.service.UfService;

@Controller
@RequestMapping("empresa")
public class EmpresaController {

	@Autowired
	private EmpresaService service;
	@Autowired
	private FiscalizacaoService fiscalizacaoService;
	@Autowired
	private CidadeService cidadeService;
	@Autowired
	private BairroService bairroService;
	@Autowired
	private UfService ufService;

	@GetMapping("/listatodos")
	public String listAll(ModelMap model) {
		model.addAttribute("empresalist", service.list());
		return "empresa/listar";
	}

	@GetMapping("/detalhes")
	public String detalhes(@RequestParam("id") String id, ModelMap model) {
		model.addAttribute("lstFis", fiscalizacaoService.getFisByEmpresa(Long.valueOf(id)));
		model.addAttribute("empresa", service.getById(Long.valueOf(id)));
		return "empresa/detalhes";
	}

	@GetMapping("/cadastrar")
	public String cadastrar(Empresa empresa, ModelMap model) {
		model.addAttribute("cidades", cidadeService.listOrdered());
		model.addAttribute("bairros", bairroService.listOrderedByUf());
		model.addAttribute("ufs", ufService.listOrdered());
		return "empresa/cadastro";
	}

	@PostMapping("/salvar")
	public String salvar(Empresa empresa, BindingResult result, RedirectAttributes attr, ModelMap model) {

		if (service.getByCnpj(empresa.getCnpj()) != null) {
			attr.addFlashAttribute("fail", "empresa já cadastrada");
			return "redirect:/empresa/cadastrar";
		}

		empresa.setEnd();
		empresa.setLastFis(LocalDate.now());

		service.persist(empresa);
		attr.addFlashAttribute("success", "Empresa inserida com sucesso");
		return "redirect:/empresa/cadastrar";
	}

	@PostMapping("/remover")
	public String remover(@RequestParam("id") String id, ModelMap model) {
		service.remove(Long.valueOf(id));
		model.addAttribute("success", "Cidade excluída com sucesso.");
		return listAll(model);
	}

}
