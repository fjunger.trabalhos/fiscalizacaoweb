package ams.trabalho02.fsma.amsFiscalizacao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ams.trabalho02.fsma.amsFiscalizacao.model.Pessoa;
import ams.trabalho02.fsma.amsFiscalizacao.service.BairroService;
import ams.trabalho02.fsma.amsFiscalizacao.service.CidadeService;
import ams.trabalho02.fsma.amsFiscalizacao.service.EmpresaService;
import ams.trabalho02.fsma.amsFiscalizacao.service.FiscalizacaoService;
import ams.trabalho02.fsma.amsFiscalizacao.service.PessoaService;
import ams.trabalho02.fsma.amsFiscalizacao.service.UfService;

@Controller
@RequestMapping("pessoa")
public class PessoaController {

	@Autowired
	private PessoaService service;
	@Autowired
	private CidadeService cidadeService;
	@Autowired
	private BairroService bairroService;
	@Autowired
	private UfService ufService;
	
	@GetMapping("/listatodos")
	public String listAll(ModelMap model) {
		model.addAttribute("pessoalist", service.list());
		return "pessoa/listar";
	}
	
	@GetMapping("/cadastrar")
	public String cadastrar(Pessoa pessoa , ModelMap model) {
		// A carga de cidades de bairros devem ser feitas por demanda
		model.addAttribute("cidades", cidadeService.listOrdered());
		model.addAttribute("bairros", bairroService.listOrderedByUf());
		model.addAttribute("ufs", ufService.listOrdered());
		return "pessoa/cadastro";
	}
	
}
