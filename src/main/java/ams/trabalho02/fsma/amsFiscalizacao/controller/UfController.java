package ams.trabalho02.fsma.amsFiscalizacao.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;
import ams.trabalho02.fsma.amsFiscalizacao.service.UfService;

@Controller
@RequestMapping("/uf")
public class UfController {

	@Autowired
	private UfService service;

	@GetMapping("/listatodos")
	public String listAll(ModelMap model) {
		model.addAttribute("uflist", service.list());
		return "uf/listar";
	}

	@GetMapping("/cadastrar")
	public String cadastrar(UnidadeFederativa ufObject) {
		return "uf/cadastro";
	}

	@PostMapping("/salvar")
	public String salvar(@Valid UnidadeFederativa ufObject, BindingResult result, RedirectAttributes attr,
			ModelMap model) {

//		if (ufValidador.naoPodeIncluir(ufObject) ) {
//			...
//			return;
//		}
		if (result.hasErrors()) {
			return "uf/cadastro";
		}

		if (verifyDuplicity(ufObject, model)) {
			return "uf/cadastro";
		}

		service.persist(ufObject);
		attr.addFlashAttribute("success", "U.F. inserido com sucesso");
		return "redirect:/uf/cadastrar";
	}

	private boolean verifyDuplicity(@Valid UnidadeFederativa ufObject, ModelMap model) {
		if (service.getByName(ufObject.getName())) {
			model.addAttribute("fail", "Já existe U.F. cadastrada com este nome: " + ufObject.getName());
			return true;
		}
		if (service.getByUf(ufObject.getUf())) {
			model.addAttribute("fail", "Já existe U.F. cadastrada com esta sigla: " + ufObject.getUf());
			return true;
		}
		return false;
	}

	@PostMapping("/remover")
	public String remover(@RequestParam("id") String id, ModelMap model) {
		if (service.ufTemCidades(Long.valueOf(id))) {
			model.addAttribute("fail", "U.F. não removida. Possui cidade(s) vinculada(s).");
		} else {
			service.remove(Long.valueOf(id));
			model.addAttribute("success", "U.F. excluído com sucesso.");
		}
		return listAll(model);
	}

	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Long id, ModelMap model) {
		model.addAttribute("unidadeFederativa", service.getById(id));
		return "uf/cadastro";
	}

	@PostMapping("/editar")
	public String editar(@Valid UnidadeFederativa uf, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "uf/cadastro";
		}
		service.persist(uf);
		attr.addFlashAttribute("success", "U.F. editado com sucesso");
		return "redirect:/uf/listatodos";
	}
}
