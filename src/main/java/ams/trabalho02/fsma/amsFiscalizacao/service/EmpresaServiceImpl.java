package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.trabalho02.fsma.amsFiscalizacao.model.Empresa;
import ams.trabalho02.fsma.amsFiscalizacao.repository.EmpresaRepo;

@Service
public class EmpresaServiceImpl implements EmpresaService{

	@Autowired private EmpresaRepo repository;
	
	@Override
	public Object list() {
		return repository.findAll();
	}

	@Override
	public Empresa getById(Long id) {
		return repository.getOne(id);
	}

	@Override
	public void persist(Empresa empresa) {
		repository.save(empresa);
	}

	@Override
	public Object getByCnpj(String cnpj) {
		return repository.findByCnpj(cnpj);
	}

	@Override
	public void remove(Long id) {
		repository.deleteById(id);
	}

}
