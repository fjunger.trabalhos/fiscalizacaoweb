package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import ams.trabalho02.fsma.amsFiscalizacao.model.Fiscalizacao;

public interface FiscalizacaoService {

	List<Fiscalizacao> getFisByEmpresa(Long id);

}
