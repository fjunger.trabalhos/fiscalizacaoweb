package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;
import ams.trabalho02.fsma.amsFiscalizacao.repository.CidadeRepo;
import ams.trabalho02.fsma.amsFiscalizacao.repository.UfRepo;

@Service
public class UfServiceImpl implements UfService{

	@Autowired private UfRepo ufRepo;
	
	@Autowired private CidadeRepo cidadeRepo;
	
	@Override @Transactional(readOnly = true)
	public List<UnidadeFederativa> list() {
		return ufRepo.findAll();
	}

	@Override @Transactional(readOnly = true)
	public UnidadeFederativa getById(Long id) {
		return ufRepo.findByid(id);
	}

	@Override
	public void persist(UnidadeFederativa uf) {
		ufRepo.save(uf);
	}

	@Override
	public void remove(Long id) {
		ufRepo.deleteById(id);
	}

	@Override @Transactional(readOnly = true)
	public boolean ufTemCidades(Long id) {
		return (cidadeRepo.findFirstByuf(ufRepo.findByid(id))!=null);
	}

	@Override
	public boolean getByName(String name) {
		return (ufRepo.findByName(name)!=null);
	}

	@Override
	public boolean getByUf(String uf) {
		return (ufRepo.findByuf(uf)!=null);
	}

	@Override
	public List<UnidadeFederativa> listOrdered() {
		return ufRepo.findByOrderByNameAsc();
	}

}
