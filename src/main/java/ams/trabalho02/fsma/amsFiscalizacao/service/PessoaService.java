package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import ams.trabalho02.fsma.amsFiscalizacao.model.Pessoa;

public interface PessoaService {

	List<Pessoa> list();

}
