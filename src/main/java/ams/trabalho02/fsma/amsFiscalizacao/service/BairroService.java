package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import javax.validation.Valid;

import ams.trabalho02.fsma.amsFiscalizacao.model.Bairro;
import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;

public interface BairroService {

	List<Bairro> getByCidade(Cidade cidade);

	Object list();

	List<Bairro> getByName(String removerCharEspec);

	Object getByUf(Long valueOf);

	Object getByCidade(Long valueOf);

	void persist(@Valid Bairro bairro);

	boolean exist(Bairro bairro);

	boolean BairroTemEmpresa(Long valueOf);

	void remove(Long id);

	Object listOrderedByUf();


}
