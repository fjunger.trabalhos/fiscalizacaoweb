package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;

public interface UfService {

	public List<UnidadeFederativa> list();
	public UnidadeFederativa getById(Long id);
	public void persist(UnidadeFederativa uf);
	public void remove(Long valueOf);
	public boolean ufTemCidades(Long valueOf);
	public boolean getByName(String name);
	public boolean getByUf(String uf);
	public Object listOrdered();
	
}
