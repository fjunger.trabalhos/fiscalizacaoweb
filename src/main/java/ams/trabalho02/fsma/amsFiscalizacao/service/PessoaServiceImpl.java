package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.trabalho02.fsma.amsFiscalizacao.model.Pessoa;
import ams.trabalho02.fsma.amsFiscalizacao.repository.PessoaRepo;

@Service
public class PessoaServiceImpl implements PessoaService{

	@Autowired
	private PessoaRepo pessoaRepo;

	@Override
	public List<Pessoa> list() {
		return pessoaRepo.findAll();
	}
}
