package ams.trabalho02.fsma.amsFiscalizacao.service;

import ams.trabalho02.fsma.amsFiscalizacao.model.Empresa;

public interface EmpresaService {

	Object list();

	Empresa getById(Long valueOf);

	void persist(Empresa empresa);

	Object getByCnpj(String cnpj);

	void remove(Long valueOf);

}
