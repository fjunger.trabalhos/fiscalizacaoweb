package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.trabalho02.fsma.amsFiscalizacao.model.Bairro;
import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;
import ams.trabalho02.fsma.amsFiscalizacao.repository.BairroRepo;
import ams.trabalho02.fsma.amsFiscalizacao.repository.CidadeRepo;
import ams.trabalho02.fsma.amsFiscalizacao.repository.EmpresaRepo;
import ams.trabalho02.fsma.amsFiscalizacao.repository.UfRepo;

@Service
public class BairroServiceImpl implements BairroService {

	@Autowired	private BairroRepo repository;
	@Autowired	private UfRepo ufRepository;
	@Autowired	private CidadeRepo cidadeRepository;
	@Autowired	private EmpresaRepo empresaRepo;

	@Override
	public List<Bairro> getByCidade(Cidade cidade) {
		return repository.findAllByCidade(cidade);
	}

	@Override
	public Object list() {
		return repository.findAll();
	}

	@Override
	public List<Bairro> getByName(String nome) {
		return repository.findByNomeContains(nome);
	}

	@Override
	public Object getByUf(Long id) {
		Optional<UnidadeFederativa> uf = ufRepository.findById(id);
		if (uf != null) {
			return repository.findAllByUf(uf);
		}
		return null;
	}

	@Override
	public Object getByCidade(Long cidade) {
		Cidade c = cidadeRepository.findByid(cidade);
		return repository.findAllByCidade(c);
	}

	@Override
	public void persist(@Valid Bairro bairro) {
		repository.save(bairro);
	}

	@Override
	public boolean exist(Bairro bairro) {
		List<Bairro> bairros = repository.findAllOrderByNome(bairro.getNome());
		for (Bairro b : bairros) {
			if (isEquals(bairro, b)) {
				return true;
			}
		}
		return false;
	}

	private boolean isEquals(Bairro bairro, Bairro b) {
		return b.getNome().equals(bairro.getNome()) && b.getCidade().equals(bairro.getCidade());
	}

	@Override
	public boolean BairroTemEmpresa(Long id) {
		if(empresaRepo.findByBairro(repository.findById(id)).size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public void remove(Long id) {
		repository.deleteById(id);
		
	}

	@Override
	public List<Bairro>  listOrderedByUf() {
		return repository.findByOrderByNome();
	}

}
