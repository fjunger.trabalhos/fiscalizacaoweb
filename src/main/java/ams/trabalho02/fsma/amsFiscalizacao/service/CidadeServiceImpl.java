package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.repository.BairroRepo;
import ams.trabalho02.fsma.amsFiscalizacao.repository.CidadeRepo;

@Service
public class CidadeServiceImpl implements CidadeService{

	@Autowired UfService ufService;
	
	@Autowired CidadeRepo cidadeRepo;
	
	@Autowired BairroRepo bairroRepo;
	
	@Override
	public List<Cidade> list() {
		return cidadeRepo.findAll();
	}

	@Override
	public Cidade getById(Long id) {
		return cidadeRepo.findByid(id);
	}

	@Override
	public void persist(Cidade cidade) {
		cidadeRepo.save(cidade);
	}

	@Override
	public void remove(Long id) {
		cidadeRepo.deleteById(id);
		
	}

	@Override
	public boolean ufTemCidades(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getByName(String nome) {
		return cidadeRepo.findByNameContains(nome);
	}

	@Override
	public Object getByUfId(Long id) {
		System.out.println(ufService.getById(id));
		return cidadeRepo.findAllByUf(ufService.getById(id));
	}

	@Override
	public List<Cidade> listOrdered() {
		return cidadeRepo.findByOrderByNameAsc();
	}

	@Override
	public Cidade getByNameAndUf(Cidade cidade) {
		return cidadeRepo.findFirstByNameAndUf(cidade.getNome(),cidade.getUf());
	}

	@Override
	public boolean cidadeTemBairros(Long id) {
		return (bairroRepo.findFirstByCidadeId(id) != null);
	}

}
