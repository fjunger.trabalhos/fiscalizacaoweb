package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.trabalho02.fsma.amsFiscalizacao.model.Fiscalizacao;
import ams.trabalho02.fsma.amsFiscalizacao.repository.FiscalizacaoRepo;

@Service
public class FiscalizacaoServiceImpl implements FiscalizacaoService{
	
	@Autowired FiscalizacaoRepo repo;

	@Override
	public List<Fiscalizacao> getFisByEmpresa(Long id) {
		return repo.findAllByEmpresaId(id);
	}

}
