package ams.trabalho02.fsma.amsFiscalizacao.service;

import java.util.List;

import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;

public interface CidadeService {
	public List<Cidade> list();
	public Cidade getById(Long id);
	public void persist(Cidade cidade);
	public void remove(Long id);
	public boolean ufTemCidades(Long id);
	public Object getByName(String nome);
	public Object getByUfId(Long valueOf);
	public Object listOrdered();
	public Cidade getByNameAndUf(Cidade cidade);
	public boolean cidadeTemBairros(Long id);
}
