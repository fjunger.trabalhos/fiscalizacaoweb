package ams.trabalho02.fsma.amsFiscalizacao.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;



@Repository
public interface CidadeRepo extends JpaRepository<Cidade, Long> {

	List<Cidade> findAll();
	
	List<Cidade> findByNameContains(String nome);

	Cidade findByid(Long id);

	Cidade findFirstByuf(UnidadeFederativa uf);
	
	void delete(Cidade cidade);

	Cidade save(Cidade cidade);

	List<Cidade>  findAllByUf(UnidadeFederativa uf);

	Cidade findFirstByNameAndUf(String nome, UnidadeFederativa uf);

	List<Cidade> findByOrderByNameAsc();

}
