package ams.trabalho02.fsma.amsFiscalizacao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ams.trabalho02.fsma.amsFiscalizacao.model.Fiscalizacao;

public interface FiscalizacaoRepo extends JpaRepository<Fiscalizacao, Long>{

	List<Fiscalizacao> findAllByEmpresaId(Long id);

}
