package ams.trabalho02.fsma.amsFiscalizacao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;

@Repository
public interface UfRepo extends JpaRepository<UnidadeFederativa, Long> {

	List<UnidadeFederativa> findAll();

	UnidadeFederativa findByid(Long id);

	UnidadeFederativa findByuf(String sigla);

	void delete(UnidadeFederativa ufObj);

	UnidadeFederativa save(UnidadeFederativa ufObj);

	UnidadeFederativa findByName(String name);

	List<UnidadeFederativa> findByOrderByNameAsc();

}
