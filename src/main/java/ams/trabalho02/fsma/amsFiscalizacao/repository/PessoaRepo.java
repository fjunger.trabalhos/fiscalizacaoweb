package ams.trabalho02.fsma.amsFiscalizacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ams.trabalho02.fsma.amsFiscalizacao.model.Pessoa;

public interface PessoaRepo extends JpaRepository<Pessoa, Long>{

}
