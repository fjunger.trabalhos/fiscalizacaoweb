package ams.trabalho02.fsma.amsFiscalizacao.repository;



import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ams.trabalho02.fsma.amsFiscalizacao.model.Bairro;
import ams.trabalho02.fsma.amsFiscalizacao.model.Empresa;

@Repository
public interface EmpresaRepo extends JpaRepository<Empresa, Long>{

	List<Empresa> findByBairro(Optional<Bairro> optional);

	Empresa findByCnpj(String cnpj);
	
}
