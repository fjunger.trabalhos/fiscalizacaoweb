package ams.trabalho02.fsma.amsFiscalizacao.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ams.trabalho02.fsma.amsFiscalizacao.model.Bairro;
import ams.trabalho02.fsma.amsFiscalizacao.model.Cidade;
import ams.trabalho02.fsma.amsFiscalizacao.model.UnidadeFederativa;

@Repository
public interface BairroRepo extends JpaRepository<Bairro, Long>{

	List<Bairro> findAllByCidade(Cidade cidade);
	
	List<Bairro> findAllOrderByNome(String nome);

	List<Bairro> findByNomeContains(String nome);

	List<Bairro> findAllByUf(Optional<UnidadeFederativa> uf);

	List<Bairro>  findByOrderByNome();

	Bairro findFirstByCidadeId(Long id);
	
}
