package ams.trabalho02.fsma.amsFiscalizacao.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tbl_cidade",
indexes = {
		@Index(name="searchcidade_by_nome",columnList="nome"),
		@Index(name="searchcidade_by_uf_nome",columnList="uf_fk,nome")
		})
public class Cidade extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 1L;

	
	@NotBlank(message = "Informe um nome.")
    @Size(min = 5, max = 30,message = "O nome da cidade dete ter entre {min} e {max} caracteres.")
	@Column(name = "nome", nullable = false,length = 30)
	private String name;
	
	@OneToOne
	@JoinColumn(name="uf_fk",nullable = false)
	private UnidadeFederativa uf;

	public Cidade() {}

	
	public String getNome() {
		return name;
	}

	public void setNome(String name) {
		this.name = name.toUpperCase()
				.replace("Ã", "A")
				.replace("Â", "A")
				.replace("Á", "A")
				.replace("À", "A")
				.replace("Õ", "O")
				.replace("Ó", "O")
				.replace("Ô", "O")
				.replace("É", "E")
				.replace("Ẽ", "E");
	}



	public UnidadeFederativa getUf() {
		return uf;
	}

	public void setUf(UnidadeFederativa uf) {
		this.uf = uf;
	}

	@Override
	public String toString() {
		return "Cidade [name=" + name + ", uf=" + uf.toString() + "]";
	}

	
	

}
