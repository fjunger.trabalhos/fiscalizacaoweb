package ams.trabalho02.fsma.amsFiscalizacao.model;

import javax.persistence.*;

@Entity
@Table(name = "tbl_pessoa")
public class Pessoa extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;
	
	@Column(length = 100)
	private String nome;
	@Column(unique = true,length = 14)
	private String cpf;
	@OneToOne
	@JoinColumn(name = "uf_fk")
	private UnidadeFederativa uf;
	@OneToOne
	@JoinColumn(name = "cidade_fk")
	private Cidade cidade;
	@OneToOne
	@JoinColumn(name = "bairro_fk")
	private Bairro bairro;
	@Column
	private String logradouro;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public UnidadeFederativa getUf() {
		return uf;
	}

	public void setUf(UnidadeFederativa uf) {
		this.uf = uf;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", cpf=" + cpf + "]";
	}
	
}
