package ams.trabalho02.fsma.amsFiscalizacao.model;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name = "tbl_empresa",
	indexes = {
			@Index(name="searchempresa_by_empresa",columnList="cnpj")
			})
public class Empresa extends AbstractEntity<Long>{

	@Column(name = "nome" , nullable = false, length = 70)
	private String name;
	@Column(name = "cnpj" , nullable = false, length = 18)
	private String cnpj;
	@Column(name = "logradouro" , nullable = false)
	private String logradouro;
	@Column(name = "cep" , nullable = false, length = 9)
	private String cep;
	@Column(name = "last_fis" , nullable = false)
	private LocalDate lastFis;
	@OneToOne
    @JoinColumn(name = "cidade_fk",nullable = false)
	private Cidade cidade;
	@OneToOne
    @JoinColumn(name = "bairro_fk",nullable = false)
	private Bairro bairro;
	@OneToOne
    @JoinColumn(name = "uf_fk",nullable = false)
	private UnidadeFederativa uf;
	
	public Empresa() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public LocalDate getLastFis() {
		return lastFis;
	}
	public void setLastFis(LocalDate lastFis) {
		this.lastFis = lastFis;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public Bairro getBairro() {
		return bairro;
	}
	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
	public UnidadeFederativa getUf() {
		return uf;
	}
	public void setUf(UnidadeFederativa uf) {
		this.uf = uf;
	}

	public void setEnd() {
		this.cidade = this.bairro.getCidade();
		this.uf = this.bairro.getUf();
	}
	
}
