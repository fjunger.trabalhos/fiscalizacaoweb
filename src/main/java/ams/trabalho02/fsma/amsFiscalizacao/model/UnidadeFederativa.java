package ams.trabalho02.fsma.amsFiscalizacao.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(name = "tbl_uf",
indexes= {
		@Index(name="searchuf_nome",columnList="nome")
		})
public class UnidadeFederativa  extends AbstractEntity<Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank(message = "O nome é obrigatório")
	@Size(min = 2, max = 20,message = "Nome deve ter entre {min} e {max} caracteres.")
	@Column(name = "nome", nullable = false, length = 20, unique = true)
	private String name;
	
	@NotBlank(message = "A sigla é obrigatório")
	@Size(min = 2, max = 2,message = "Sigla deve ter apenas {min} caracteres.")
	@Column(name = "sigla", nullable = false, length = 2)
	private String uf;
	
	public UnidadeFederativa(String name, String sigla) {
		this.name=name;
		this.uf=sigla;
	}
	
	public UnidadeFederativa() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toUpperCase();
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf.toUpperCase();
	}

	@Override
	public String toString() {
		return "Uf [name=" + name + ", uf=" + uf + "]";
	}
	
}
