package ams.trabalho02.fsma.amsFiscalizacao.model;

import javax.persistence.*;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_bairro",
indexes = {
		@Index(name="searchbairro_by_name",columnList="nome")
		})
public class Bairro extends AbstractEntity<Long>{

	@Column(name = "nome", nullable = false, length = 20)
	private String nome;
	
	@OneToOne
    @JoinColumn(name = "uf_fk",nullable = false)
	private UnidadeFederativa uf;
	
	@OneToOne
	@JoinColumn(name = "cidade_fk",nullable = false)
	private Cidade cidade;

	public String getNome() {
		return nome;
	}

	public void setNome(String name) {
		this.nome = name;
	}

	public UnidadeFederativa getUf() {
		return uf;
	}

	public void setUf(UnidadeFederativa uf) {
		this.uf = uf;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
}
